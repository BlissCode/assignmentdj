//
//  BeatMatch.cpp
//  JuceBasicWindow - App
//
//  Created by Tom Blissett on 30/01/2020.
//

#include "BeatMatch.h"

BeatMatch::BeatMatch()
{
    // playback speed slider in GUI
    bpmSlider1.addListener (this);
    bpmSlider1.setSliderStyle(Slider::LinearVertical);
    bpmSlider1.setRange(0.01f, 2.0f);
    bpmSlider1.setValue(1.0);
    addAndMakeVisible(&bpmSlider1);
    
    // adding listener for buttons and making them visible on GUI
    tempoReset.addListener(this);
    addAndMakeVisible(&tempoReset);
    
    tempoLock.addListener(this);
    addAndMakeVisible(&tempoLock);
}

BeatMatch::~BeatMatch()
{
    
}
void BeatMatch::setFilePlayer (FilePlayer* fp)
{
    beatMatchControl = fp;
}

void BeatMatch::resized()
{
    // setting bounds for playback speed slider and tempo buttons
    bpmSlider1.setBounds(0, 150, getWidth()/2, getHeight()/2);
    tempoReset.setBounds(107, 350, getWidth()/8, getHeight()/10);
    tempoLock.setBounds(107, 50, getWidth()/8, getHeight()/10);
}

void BeatMatch::sliderValueChanged(Slider* slider)
{
    if (slider == &bpmSlider1 && beatMatchControl != nullptr)       // triggered when the playback speed slider position is changed
    {
        float newRate = bpmSlider1.getValue();
        beatMatchControl->setPlaybackRate(newRate);         // sets playback speed to current value of playback speed slider
    }
}

void BeatMatch::buttonClicked(Button* button)
{
    if (button == &tempoReset && beatMatchControl != nullptr)       // triggered when tempo reset button is pressed
    {
        double reset = 1.0;
        beatMatchControl->setPlaybackRate(reset);                // sets the playback speed to the default value of 1.0
        bpmSlider1.setValue(reset);                             // sets the playback speed slider position back to 1.0
    }
}

void BeatMatch::buttonStateChanged(Button* button)
{
    bool toggleState = tempoLock.getToggleState();
    
    if (button == &tempoLock && beatMatchControl != nullptr && toggleState == true)   // triggered when tempoLock button is toggled on
    {
        double lockValue = bpmSlider1.getValue();
        bpmSlider1.removeListener(this);
        bpmSlider1.setValue(lockValue);                        // removes listener and keeps playback speed at last recorded value
    }
    else if (button == &tempoLock && beatMatchControl != nullptr && toggleState == false)  // triggered when tempoLock is toggled off
    {
        bpmSlider1.addListener(this);
        double unlock = bpmSlider1.getValue();          // adds listener and puts playback speed at whatever value the playback speed
        beatMatchControl->setPlaybackRate(unlock);     //  slider is currently on
    }
}
