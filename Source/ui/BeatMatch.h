//
//  BeatMatch.h
//  JuceBasicWindow - App
//
//  Created by Tom Blissett on 30/01/2020.
//

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "../audio/FilePlayer.h"
#include "../audio/Audio.h"

//==============================================================================
/**
 The base class for all GUI objects that affect playback speed.
 
 @tags{GUI}
 */

class BeatMatch : public Component,
                  public Slider::Listener,
                  public Button::Listener
{
public:
    /** Constructor */
    BeatMatch();
    
    /** Destructor */
    ~BeatMatch();
    
    /** sets the file player that this gui controls */
    void setFilePlayer (FilePlayer* fp);
    
    //Component
    void resized() override;
    
    /** Called when the slider's value is changed. */
    void sliderValueChanged (Slider* slider) override;
    
    /** Called when the button is clicked. */
    void buttonClicked (Button* button) override;
    
    /** Called when the button's state has changed */
    void buttonStateChanged (Button* button) override;
    
private:
    
    // playback speed slider
    Slider bpmSlider1;
    
    // tempo reset and lock buttons
    TextButton tempoReset {"Reset"};
    ToggleButton tempoLock {"Lock"};
    
    // pointer to FilePlayer class
    FilePlayer* beatMatchControl {nullptr};
    
};


