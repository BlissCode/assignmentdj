/*
  ==============================================================================
    FilePlayerGui.h
  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "../audio/FilePlayer.h"

//==============================================================================
/**
 The base class for all GUI objects that affect the file player playback position and playing state.
 
 @tags{GUI}
 */

class FilePlayerGui :   public Component,
                        public Button::Listener,
                        private FilenameComponentListener,
                        public Slider::Listener
{
public:
    /** Constructor */
    FilePlayerGui();
    
    /** Destructor */
    ~FilePlayerGui();
    
    /** sets the file player that this gui controls */
    void setFilePlayer (FilePlayer* fp);
    
    // Component
    void resized() override;
    
    /** Called when the slider's value is changed. */
    void sliderValueChanged (Slider* slider) override;
    
    /** Called when the button is clicked. */
    void buttonClicked (Button* button) override;

private:

    // cue and play buttons
    TextButton cueButton;
    TextButton playButton {">||"};
    
    // playback position slider
    Slider playbackPos;
    
    // FilenameComponentListener
    void filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged) override;

    // File chooser
    std::unique_ptr<FilenameComponent> fileChooser;
    
    // pointer to FilePlayer class
    FilePlayer* filePlayer {nullptr};

};
