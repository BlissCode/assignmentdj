//
//  MixerGui.hpp
//  JuceBasicWindow - App
//
//  Created by Tom Blissett on 28/01/2020.
//

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "../audio/FilePlayer.h"
#include "../audio/Audio.h"

//==============================================================================
/**
 The base class for all GUI objects that affect file player volume.
 
 @tags{GUI}
 */

class MixerGui : public Component,
                 public Slider::Listener
{
public:
    /** Constructor */
    MixerGui();
    
    /** Destructor */
    ~MixerGui();
    
    /** sets the file player that this gui controls */
    void setFilePlayer (FilePlayer* fp);

    //Component
    void resized() override;

    /** Called when the slider's value is changed. */
    void sliderValueChanged (Slider* slider) override;
    
private:
    // volume sliders
    Slider sliderVolume;
    Slider sliderVolume2;
    
    // volume crossfader
    Slider crossFader;
    
    // pointer to FilePlayer class
    FilePlayer* mixerControl {nullptr};
    
};
