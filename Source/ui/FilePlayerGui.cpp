/*
  ==============================================================================

    FilePlayerGui.cpp
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "FilePlayerGui.h"

FilePlayerGui::FilePlayerGui()
{
    // Adding play button in GUI + adding listener
    playButton.addListener (this);
    addAndMakeVisible (playButton);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = std::make_unique<FilenameComponent> ("audiofile",
                                                       File(),
                                                       true, false, false,
                                                       formatManager.getWildcardForAllFormats(),
                                                       String(),
                                                       "(select an audio file)");
    fileChooser->addListener (this);
    addAndMakeVisible (fileChooser.get());
    
    // Adding playback position slider in GUI + adding liastener
    playbackPos.addListener (this);
    playbackPos.setSliderStyle(Slider::LinearHorizontal);
    playbackPos.setRange (0.f, 300.f);
    addAndMakeVisible (&playbackPos);

    // Adding cue Button in GUI + adding listener
    cueButton.addListener(this);
    cueButton.setButtonText("Cue");
    addAndMakeVisible(&cueButton);

}

FilePlayerGui::~FilePlayerGui()
{
    
}

//Component
void FilePlayerGui::resized()
{
    int height = getHeight()/2;
    int width = getWidth()/2;

    // File Player bounds
    playButton.setBounds (300, 650, width/4, height/15);
    fileChooser->setBounds (0, 30, (getWidth() - 20)/2, height/15);
    
    // Playback position slider bounds
    playbackPos.setBounds(0, 50, width, height/4);
    
    // Cue button bounds
    cueButton.setBounds(100, 650, width/4, height/15);
}

//Button Listener
void FilePlayerGui::buttonClicked (Button* button)
{
    if (filePlayer != nullptr && button == &playButton)     // triggered when the play button is pressed
    {
        filePlayer->setPlaying( ! filePlayer->isPlaying());         // plays audio, pauses if it is already playing
    }
    else if (filePlayer != nullptr && button == &cueButton)     // triggered when the cue button is called
    {
        double newPosition = 0.0;
        filePlayer->setPosition(newPosition);        // sets position of playback to the beginning after cue has been pressed
        filePlayer->setPlaying(false);              // stops file playing after cue button pressed
        playbackPos.setValue(newPosition);         // resets playback position slider to 0.0 when cue button is pressed
    }
}

//Slider listener
void FilePlayerGui::sliderValueChanged(Slider* slider)
{
    if (filePlayer != nullptr && slider == &playbackPos)        // triggered when the position of playback slider is changed
    {
        double newPosition = playbackPos.getValue();
        filePlayer->setPosition(newPosition);           //takes value from playback slider and puts it into a function in FilePlayer.h
    }
    
}

void FilePlayerGui::setFilePlayer (FilePlayer* fp)
{
    filePlayer = fp;
}

//FilenameComponentListener
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser.get())
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(filePlayer != nullptr && audioFile.existsAsFile())
        {
            filePlayer->loadFile(audioFile);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}
