//
//  MixerGui.cpp
//  JuceBasicWindow - App
//
//  Created by Tom Blissett on 28/01/2020.
//

#include "MixerGui.h"

MixerGui::MixerGui()
{
    // adding volume sliders in GUI + listeners for sliders
    sliderVolume.addListener (this);
    sliderVolume.setSliderStyle(Slider::LinearVertical);
    sliderVolume.setRange(0.00, 1.0);
    addAndMakeVisible(&sliderVolume);
    sliderVolume.setValue(1.0);
    
    sliderVolume2.addListener (this);
    sliderVolume2.setSliderStyle(Slider::LinearVertical);
    sliderVolume2.setRange(0.00, 1.0);
    addAndMakeVisible(&sliderVolume2);
    sliderVolume2.setValue(0.0);
    
    // adding volume crossfader in GUI + listener for crossfader slider
    crossFader.addListener (this);
    crossFader.setSliderStyle(Slider::LinearHorizontal);
    crossFader.setRange(-0.15, 0.15);
    addAndMakeVisible(&crossFader);
    crossFader.setValue(0.0);
}

MixerGui::~MixerGui()
{
    
}

void MixerGui::setFilePlayer (FilePlayer* fp)
{
    mixerControl = fp;
}

void MixerGui::resized()
{
    int width = getWidth();
    int height = getHeight();
    
    // setting bounds for sliders in MixerGui
    sliderVolume.setBounds(500, 150, width/20, height/2);
    sliderVolume2.setBounds(570, 150, width/20, height/2);
    crossFader.setBounds(465, 350, width/8, height/8);
}

void MixerGui::sliderValueChanged(Slider* slider)
{
    if (slider == &sliderVolume && mixerControl != nullptr)           // triggered when left deck volume slider position is changed
    {
        double newGain = sliderVolume.getValue();
        mixerControl->setGain(newGain);                 // sending volume slider value to setGain function in FilePlayer.h
    }
    else if (slider == &crossFader && mixerControl != nullptr)
    {
        // extract values from volume sliders and cross faders
        double faderPos = crossFader.getValue()/5.0;                // divided by 5 to reduce sharpness of crossfade
        double vol1Pos = sliderVolume.getValue();
        double vol2Pos = sliderVolume2.getValue();
        
        // allows crossfader to affect volume sliders values
        double fadeToLeftDeck = vol1Pos - faderPos;          // when crossfader is left, left deck volume at 1, right deck at 0
        double fadeToRightDeck = vol2Pos + faderPos;        // when crossfader is right, right deck volume at 1, left deck at 0
        
        // set volume sliders values to allow crossfader to have an effect
        sliderVolume.setValue(fadeToLeftDeck);
        sliderVolume2.setValue(fadeToRightDeck);                // sets slider values as calculations above
    }
}

