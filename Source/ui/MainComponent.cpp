/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainComponent::MainComponent (Audio& a) :   audio (a)
{
    setSize (600, 400);
    
    // controlling which file player the GUI will control
    deckLeft.setFilePlayer(audio.getFilePlayer());
    deckRight.setFilePlayer(audio.getFilePlayer());
    mixer.setFilePlayer(audio.getFilePlayer());
    beatMatchLeft.setFilePlayer(audio.getFilePlayer());
    beatMatchRight.setFilePlayer(audio.getFilePlayer());
    
    // making all GUI components visible on the window
    addAndMakeVisible(&deckLeft);
    addAndMakeVisible(&deckRight);
    addAndMakeVisible(&mixer);
    addAndMakeVisible(&beatMatchLeft);
    addAndMakeVisible(&beatMatchRight);
                        
}

MainComponent::~MainComponent()
{
    
}

//==============================================================================
void MainComponent::resized()
{
    // setting the bounds for all GUI components
    deckLeft.setBounds(10, 10, getWidth()/1.1, getHeight());
    deckRight.setBounds(750, 10, getWidth()/1.1, getHeight());
    mixer.setBounds(120, 150, getWidth(), getHeight()/2);
    beatMatchLeft.setBounds(0, 150, getWidth()/4, getHeight()/2);
    beatMatchRight.setBounds(1250, 150, getWidth()/4, getHeight()/2);

}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0};
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Preferences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            2, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}
