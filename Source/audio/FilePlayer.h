/*
  ==============================================================================
    FilePlayer.h
  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/**
 The base class for all objects related to FilePlayer, which streams the audio.
 
 */

/** Simple FilePlayer class - streams audio from a file. */
class FilePlayer :  public AudioSource

{
public:
    /** Constructor */
    FilePlayer();
    
    /** Destructor */
    ~FilePlayer();
    
    /** Starts or stops playback of the looper */
    void setPlaying (bool newState);
    
    /** Gets the current playback state of the looper */
    bool isPlaying() const;
    
    /** Loads the specified file into the transport source */
    void loadFile (const File& newFile);
    
    //==============================================================================
    /** Changes the current playback position in the source stream.
     
     Value is passed from FilePlayerGui.h to give the newPosition value.
     
     @param newPosition    the new playback position in seconds
     
     @see getCurrentPosition
     */
    void setPosition (double newPosition){
        audioTransportSource.setPosition(newPosition);
    };
    
    /** Gets current playback position of source */
    void getPosition (double currentPosition){
        
    };
    
    //==============================================================================
    /** Changes the playback speed of the source stream.
     
     Value from newRate is used to change the resampling ratio for ResamplingAudioSource. This value can be changed at any time, even
     while the source isrunning.
     
     @param newRate   a factor to multiply the playback rate by.
     */
    void setPlaybackRate(double newRate){
        double newValue = newRate;
        resamplingAudioSource->setResamplingRatio(newValue);
    };
    
     //==============================================================================
     /** Changes the gain to apply to the output.
      
      Value is passed from MixerGui.h to give the newGain value.
      
     @param newGain  a scale factor by which to multiply the outgoing samples.
      
     @see getGain
     */
    void setGain(double newGain){
        audioTransportSource.setGain(newGain);
    };
    
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
    
    void releaseResources() override;
    
    /** Called repeatedly to fetch subsequent blocks of audio data.
     
     After calling the prepareToPlay() method, this callback will be made each
     time the audio playback hardware (or whatever other destination the audio
     data is going to) needs another block of data.
     
     @see AudioSourceChannelInfo, prepareToPlay, releaseResources
     */
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override;
    
private:
    std::unique_ptr<AudioFormatReaderSource> currentAudioFileSource;    //reads audio from the file
    AudioTransportSource audioTransportSource;
    
                                                        // this controls the playback of a positionable audio stream, handling the
                                                        // starting/stopping and sample-rate conversion
    TimeSliceThread thread;                             //thread for the transport source
    
    std::unique_ptr<ResamplingAudioSource> resamplingAudioSource;       //unique pointer to ResamplingAudioSource class
};
